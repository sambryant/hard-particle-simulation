\documentclass[12pt]{article}
\usepackage{hyperref}
\usepackage{sjbsymbols}
\usepackage{tabularx}
\usepackage{multicol}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}

\pagestyle{headings}
\setlength{\parindent}{0in}

\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,
    urlcolor=cyan,
}

\title{Algorithms for Hard Particle Simulation}

\newcommand{\dt}{\d t}
\newcommand{\ka}{k_a}
\newcommand{\kb}{k_b}
\newcommand{\kx}{k_x}
\newcommand{\ky}{k_y}
\newcommand{\sgx}{\sg_x}
\newcommand{\sgy}{\sg_y}
\newcommand{\kk}{|\bb k|}
\newcommand{\Power}{\tn{Power}}
\newcommand{\Diss}{\<\tn{Diss}\>}
\newcommand{\Signal}{\tn{Signal}}
\newcommand{\Noise}{\tn{Noise}}
\newcommand{\Cost}{\f{\tn{Cost}}{\tn{bit}}}

%% Paradigm labeling commands
\newcommand{\difftwo}{\tn{D2}}
\newcommand{\diffthr}{\tn{D3}}
\newcommand{\electric}{\tn{El}}
\newcommand{\phases}{\tn{Ph}}
\newcommand{\waves}{\tn{W2}}
\newcommand{\sound}{\tn{W3}}


\begin{document}
\maketitle

\section{Collision Time Calculation}
When two particles collide in a given timestep, we want to know {\it exactly} what sub-time they collided in. We do this by calculating $f\in [0,\d t]$ such that the particles are exactly $r^2$ distance apart:
\[
  x^{(i)}(f) = x^{(i)}_0 + f v_x^{(1)}
\]
The collision time equation:
\begin{align*}
  [x^{(1)}(f) - x^{(2)}(f)]^2 + [y^{(1)}(f)-y^{(2)}(f)]^2 = (2r)^2
\end{align*}
Doing out the math:
\begin{align*}
  4r^2
  &=
  [x^{(1)}_0 - x^{(2)}_0 + f(v_x^{(1)}-v_x^{(2)})]^2
  +
  [y^{(1)}_0 - y^{(2)}_0 + f(v_y^{(1)}-v_y^{(2)})]^2
  \\
  &=
  f^2(\d v_x^2 + \d v_y^2) + 2f(\d x \d v_x + \d y \d v_y) + \d x^2 + \d y^2
\end{align*}
Solving the quadratic:
\begin{align*}
  f
  &= \f{1}{(\d v_x^2 + \d v_y^2)}\l[-(\d x \d v_x + \d y \d v_y)
    +
    \sqrt{(\d x \d v_x + \d y \d v_y)^2 - (\d v_x^2+\d v_y^2)(\d x^2+\d y^2-4r^2)}
  \r]
  \\
  &= \f{(-\d x \d v_x - \d y \d v_y)}{(\d v_x^2 + \d v_y^2)}\l[
    1
    +
    \sqrt{1 - \f{(\d v_x^2+\d v_y^2)(\d x^2+\d y^2-4r^2)}{(\d x\d v_x + \d y \d v_y)^2}}
  \r]
\end{align*}
So practically speaking you compute three things:
\begin{align*}
  \d x^2 + \d y^2
  &&
  \d v_x^2 + \d v_y^2
  &&
  \d x\d v_x + \d y \d v_y
\end{align*}


\section{Two-Body Kinetics Problem}
We work in the frame where the center of mass is not moving:
\begin{align*}
  \t x(t) = x - v_c t
  &&
  \t v = v - v_c
\end{align*}
We denote $\bb x_1$ and $\bb x_2$ to be the particle coordinates {\it at the time of collision}. The angle from $(1)$ to $(2)$ is given by:
\[
  \tan\th = \f{y_2 - y_1}{x_2 - x_1}
\]
Then the component of velocity in the $\th$ direction is inverted. So we first rotate the velocity:
\[
  \bb{\t v}^{r} = R^{-1}(\th)\bb{\t v}
\]
Then we invert it, and then we rotate back:
\[
  \bb{\t v}' = R(\th)\mat{cc}{-1 & 0 \\ 0 & 1} R^{-1}(\th)\bb{\t v}
\]
So in matrix form:
\begin{align*}
  \bb{\t v}^{(i)}_f
  &=
    \mat{cc}{\cos\th & -\sin\th \\ +\sin\th & \cos\th}
    \mat{cc}{-1 & 0 \\ 0 & +1}
    \mat{cc}{\cos\th & +\sin\th \\ -\sin\th & \cos\th}
    \mat{c}{v^{(i)}_x - v^{(c)}_x  \\ v^{(i)}_y - v^{(c)}_y}
  \\
  &=
    \mat{cc}{
      -(\cos^2\th - \sin^2\th) & -2\cos\th\sin\th \\
      -2\cos\th\sin\th & (\cos^2\th - \sin^2\th)}
    \mat{c}{v^{(i)}_x - v^{(c)}_x  \\ v^{(i)}_y - v^{(c)}_y}
\end{align*}
We can make this more explicit by writing the velocity using a three vector:
\begin{align*}
  \bb v^{(i)} = \mat{c}{v^{(i)}_x \\ v^{(i)}_y \\ 1}
  &&
  T_c = \mat{ccc}{
    1 & 0 & v_x^{(c)} \\
    0 & 1 & v_y^{(c)} \\
    0 & 0 & 1
  }
\end{align*}
Then the full map is:
\[
  \bb v_f = T_c R(\th)\mat{cc}{-1 & 0 \\ 0 & 1}R^{-1}(\th)T^{-1}_c \bb v_i
\]
Solving this matrix transformation yields:
% \begin{align*}
%   M
%   &=
%   \mat{ccc}{
%     1 & 0 & v_x^{(c)} \\
%     0 & 1 & v_y^{(c)} \\
%     0 & 0 & 1
%   }
%   \mat{ccc}{
%     -(\cos^2\th - \sin^2\th) & 2\cos\th\sin\th & 0\\
%     2\cos\th\sin\th & (\cos^2\th - \sin^2\th) & 0\\
%     0 & 0 & 1
%   }
%   \mat{ccc}{
%     1 & 0 & -v_x^{(c)} \\
%     0 & 1 & -v_y^{(c)} \\
%     0 & 0 & 1
%   }
%   \\
%   &=
%   \mat{ccc}{
%     1 & 0 & v_x^{(c)} \\
%     0 & 1 & v_y^{(c)} \\
%     0 & 0 & 1
%   }
%   \mat{ccc}{
%     -(\cos^2 - \sin^2) & 2\cos\sin & +v^c_x(\cos^2 - \sin^2) - 2v^c_y\cos\sin \\
%     2\cos\sin & +(\cos^2 - \sin^2) & -v_y^c(\cos^2 - \sin^2) - 2v^c_x\cos\sin \\
%     0 & 0 & 1
%   }
%   \\
%   &=
%   \mat{ccc}{
%     -(\cos^2-\sin^2) & +2\cos\sin & +v^c_x(\cos^2-\sin^2) - 2v^c_y\cos\sin + v_x^c \\
%     +2\cos\sin & +(\cos^2-\sin^2) & -v_y^c(\cos^2-\sin^2) - 2v^c_x\cos\sin + v_y^c \\
%     0 & 0 & 1
%   }
% \end{align*}
\begin{equation}
  M
  =
  \mat{ccc}{
    -(\cos^2-\sin^2) & -2\cos\sin & v^c_x\l[1 + (\cos^2-\sin^2)\r] + 2v^c_y\cos\sin\\
    -2\cos\sin & +(\cos^2-\sin^2) & v^c_y\l[1 - (\cos^2-\sin^2)\r] + 2v^c_x\cos\sin\\
    0 & 0 & 1
  }
\end{equation}
So just to reiterate. We compute $\th$ via:
\begin{equation}
  \tan\th = \f{\d y}{\d x}
\end{equation}
Then we compute the center of mass velocity:
\begin{equation}
  \bb v^{(c)} = \bb v^{(1)} + \bb v^{(2)}
\end{equation}
Then using these two quantities, we compute the matrix $M$.
Then finally we set the new velocities by just multiplying the velocity vectors by $M$.



\subsection{Simple Test Case}
Consider the collision where $r=1.0$ occuring at:
\begin{align*}
  \bb x^{(1)}_f = (-2, 0)
  &&
  \bb x^{(2)}_f = (0, 0)
  &&
  \bb v^{(1)}_f = (1, 0)
  &&
  \bb v^{(2)}_f = (0, 0)
\end{align*}
Going by the algorithm I have layed out:
\[
  \th = \tn{atan2}(0, -1) = \pi
\]
The stationary particle (2) in the CM frame is:
\[
  \bb{\t v}^{(2)}_f
  = \mat{c}{0 \\ 0} - \mat{c}{\f{1}{2} \\ 0}
  = \mat{c}{-\f{1}{2} \\ 0}
\]
Then we rotate into the plane of incidence:
\[
  \bb{\t v}^{(2)}_r
  = \mat{cc}{\cos\pi & -\sin\pi \\ \sin\pi & \cos\pi}\mat{c}{-\f{1}{2} \\ 0}
  = \mat{c}{\f{1}{2} \\ 0}
\]
Then we flip the transformed x velocity:
\[
  \bb{\t v}^{(2)}_r
  \to \mat{cc}{-1 & 0 \\ 0 & +1}\mat{c}{\f{1}{2} \\ 0}
  = \mat{c}{-\f{1}{2} \\ 0}
\]
Then we undo our rotation:
\[
  \bb{\t v}^{(2)}_r
  \to \mat{cc}{\cos\pi & +\sin\pi \\ -\sin\pi & \cos\pi}\mat{c}{-\f{1}{2} \\ 0}
  = \mat{c}{\f{1}{2} \\ 0}
\]
Then we undo our CM frame:
\[
  \bb{v}^{(2)}
  \to \mat{c}{\f{1}{2} \\ 0} + \mat{c}{\f{1}{2} \\ 0}
  = \mat{c}{1 \\ 0}
\]
Which implies that all of the mass transfers into the second particle. How about the other particle? Well:
\[
  \bb{\t v}^{(1)}
  = \mat{c}{1\\0} - \mat{c}{\f{1}{2}\\0}
  = \mat{c}{\f{1}{2}\\0}
  = -\bb{\t v}^{(2)}
\]
So we can already jump to second to last step:
\[
  \bb{\t v}^{(1)} \to \mat{c}{-\f{1}{2}\\0}
\]
Then we undo our CM rotation:
\[
  \bb{v}^{(1)} \to \mat{c}{-\f{1}{2}\\0} + \mat{c}{\f{1}{2}\\0} = \mat{c}{0\\0}
\]






\section{Two-Body Kinetics Problem}
We have two particles at given points with given velocities. Using the previous algorithm we can determine where and when they collide. Now we want to know, after the collision, what will the outgoing velocities be? The trick is that we have four equations (2 conservation of momentum, 1 conservation of energy, and 1 based on collision angle) and four unknowns.

We work in coordinates where particle 1 is stationary:
\begin{align*}
  \bb {\t r} = \bb r - \bb r_0^{(1)}
  &&
  \bb {\t v} = \bb v - \bb v_0^{(1)}
\end{align*}
We denote $\et$, $\nu$ to be the velocities of particle 1 and 2 in the relativistic frame:
\begin{align*}
  \bb \et_0 = \bb 0
  &&
  \bb \nu_0 = \bb v_0^{(2)} - \bb v_0^{(1)}
\end{align*}
We introduce the (classically) relativistic initial momentum and energy:
\begin{align*}
  \bb p_0 = \bb{\t \nu}_0
  &&
  K_0 = \bb{\t \nu}^2_0
\end{align*}
We need to solve this problem in four variables:
\begin{align*}
  \et
  &&
  \th
  &&
  \nu_x
  &&
  \nu_y
\end{align*}
Since particle one is at the origin in the moving frame, the collision angle is just the angle of particle 2's position at the collision time. Then the angle that particle one moves at (in the moving frame) is just going to be this angle plus $\pi$. Let's assume we can calculate this just fine and continue:
\begin{align*}
  p_{0,x} = \et\cos\th + \nu_x
  &&
  \Rightarrow
  &&
  \et = \f{p_{0,x} - \nu_x}{\cos\th}
\end{align*}
Then we have:
\begin{align*}
  p_{0,y}
  = \et\sin\th + \nu_y
  = (p_{0,x}-\nu_x)\tan\th + \nu_y
  &&
  \Rightarrow
  &&
  \nu_y = p_{0,y} - (p_{0,x}-\nu_x)\tan\th
\end{align*}
Then we assert conservation of momentum:
\begin{align*}
  K_0 = \et^2 + \nu_x^2+\nu_y^2
  =
  \f{1}{\cos^2\th}(p_{0,x}-\nu_x)^2
  + \nu_x^2 + \l[\nu_x\tan\th + p_{0,y} - p_{0,x}\tan\th\r]^2
\end{align*}
\begin{align*}
  0
  &=
  \nu_x^2\l[\f{1}{\cos^2\th} + \tan^2\th + 1\r]
  +
  \nu_x\l[-\f{2p_x}{\cos^2\th} +2\tan\th\l(p_y - p_x\tan\th\r)\r]
  + \f{p_x^2}{\cos^2\th}+(p_y-p_x\tan\th)^2-K_0^2
  \\
  0
  &=
  \nu_x^2\f{2}{\cos^2\th}
  +
  \nu_x\l[-2p_x\f{\sin^2\th+1}{\cos^2\th}
    +2p_y\tan\th\r]
    +p_x^2\f{1+\sin^2\th}{\cos^2\th}
    -2p_xp_y\tan\th
    +p_y^2-K_0^2
\end{align*}

Let's try a different approach. Let's work in frame where center of mass is stationary. Then we have:
\begin{align*}
  \nu_x = -\et_x
  &&
  \nu_y = -\et_y
\end{align*}
We are really solving for two variables in this case: the angle and the magnitude. The magnitude comes from:
\[
  2\nu^2 = K
\]
The angle is the tricky one. Here we have to go into the frame where particle 1 is stationary, compute the interaction angle, then convert that angle into the frame where the momentum is zero, compute the velocity magnitude, then go back to the original frame.

\end{document}



