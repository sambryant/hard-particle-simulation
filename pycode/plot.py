# TO MAKE GIF AFTERWARDS:
# convert -delay 10 ./frame-*.png ./movie.gif
# To convert to MP4:
# ffmpeg -i movie.gif -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" movie.mp4

import sys
import h5py
import numpy as np
import matplotlib.pyplot as plt
import progressbar
from matplotlib.patches import Rectangle, Circle
from matplotlib.animation import FuncAnimation
from matplotlib.collections import LineCollection

FIGSIZE = (5, 5)
BACKGROUND = '#c7d2d9'
COLORS = ['#C22924', '#F2D64D', '#4D8ED3', '#D531AD', '#D58031']
TITLE_SIZE = 16
LEGEND_SIZE = 12
FRAME_SKIP = 1
LINE_SIZE = 0.033

def plot_movie_with_order(filename):
  plt.rc('text', usetex=True)
  fig, ax = plt.subplots(1, 1, figsize=FIGSIZE)

  f = h5py.File(filename)

  time = np.array(f['time'])
  pos = np.array(f['pos'])
  vel = np.array(f['vel'])

  # print(pos[0,43])
  # print(pos[0,53])

  bounds = np.array(f['bounds'])
  radius = np.array(f['radius'])[0]
  dt = time[1] - time[0]
  num_parts = len(pos[0])
  num_times = len(pos)

  ax.set_xlim(bounds[0,0], bounds[0,1])
  ax.set_ylim(bounds[1,0], bounds[1,1])

  # Movie plot:
  ax.add_artist(Rectangle((bounds[0,0], bounds[1,0]), bounds[0,1]-bounds[0,0], bounds[1,1]-bounds[0,0], color=BACKGROUND))
  artists = []

  for i in range(num_parts):
    c = plt.Circle(pos[0,i], radius=radius, color='black')
    artists.append(c)
    ax.add_artist(c)

  def animate_update(frame):
    t = frame * FRAME_SKIP
    for i in range(num_parts):
      artists[i].center = pos[frame,i]
    return artists

  plt.tight_layout()

  interval = 1000.0 * dt
  ani = FuncAnimation(fig, animate_update, interval=interval, frames=num_times, blit=True, repeat=False)
  ani.save('%s.mp4' % filename)

def plot_analytics(filename):
  plt.rc('text', usetex=True)
  fig, ax = plt.subplots(1, 1, figsize=FIGSIZE)

  f = h5py.File(filename)

  time = np.array(f['time'])
  pos = np.array(f['pos'])
  vel = np.array(f['vel'])
  bounds = np.array(f['bounds'])
  radius = np.array(f['radius'])[0]
  dt = time[1] - time[0]
  num_parts = len(pos[0])
  num_times = len(pos)

  # Compute kinetic energy at each time
  energies = np.einsum('ijk,ijk->ij', vel, vel)
  kinetic_energy = energies.sum(axis=1)

  ax.set_xlim(min(time), max(time) + dt)
  ax.set_ylim(0.0, 1.1 * max(kinetic_energy))
  ax.plot(time, kinetic_energy)
  plt.show()

if __name__ == '__main__':
  input_file = sys.argv[1]
  # plot_analytics(input_file)
  plot_movie_with_order(input_file)
