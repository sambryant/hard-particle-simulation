use std::fmt::Debug;
use std::ops::{Add, Sub, Mul};

#[derive(hdf5::H5Type, Debug, Clone, Copy)]
#[repr(C)]
pub struct Vector {
  pub x: f64,
  pub y: f64,
}

#[derive(hdf5::H5Type, Debug, Clone, Copy)]
#[repr(C)]
pub struct Point {
  pub x: f64,
  pub y: f64,
}

pub fn mat_vec_mul3(m: [[f64; 3]; 3], v: [f64; 3]) -> [f64; 3] {
  [
    m[0][0]*v[0] + m[0][1]*v[1] + m[0][2]*v[2],
    m[1][0]*v[0] + m[1][1]*v[1] + m[1][2]*v[2],
    m[2][0]*v[0] + m[2][1]*v[1] + m[2][2]*v[2]
  ]
}

impl Point {
  pub fn new(x: f64, y: f64) -> Self {
    Self { x, y }
  }

  pub fn bound_periodic(&mut self, x_max: f64, y_max: f64) {
    if self.x < 0.0 || self.x >= x_max {
      self.x = self.x.rem_euclid(x_max);
    }
    if self.y < 0.0 || self.y >= y_max {
      self.y = self.y.rem_euclid(y_max);
    }
  }
}

impl Vector {

  pub fn new(x: f64, y: f64) -> Self {
    Self { x, y }
  }

  pub fn norm(&self) -> f64 {
    (self.x * self.x + self.y * self.y).sqrt()
  }

  pub fn dot(&self, other: &Self) -> f64 {
    self.x * other.x + self.y * other.y
  }

}

impl Add for Point {
  type Output = Point;

  fn add(self, other: Self) -> Self::Output {
    Point { x: self.x + other.x, y: self.y + other.y }
  }
}

impl Add for &Point {
  type Output = Point;

  fn add(self, other: Self) -> Self::Output {
    Point { x: self.x + other.x, y: self.y + other.y }
  }
}

impl Add for Vector {
  type Output = Vector;

  fn add(self, other: Self) -> Self::Output {
    Vector { x: self.x + other.x, y: self.y + other.y }
  }
}

impl Add for &Vector {
  type Output = Vector;

  fn add(self, other: Self) -> Self::Output {
    Vector { x: self.x + other.x, y: self.y + other.y }
  }
}

impl Sub for &Point {
  type Output = Vector;

  fn sub(self, other: Self) -> Self::Output {
    Vector { x: self.x - other.x, y: self.y - other.y }
  }

}

impl Add<&Vector> for &Point {
  type Output = Point;

  fn add(self, other: &Vector) -> Self::Output {
    Point::new(self.x + other.x, self.y + other.y)
  }
}

impl Add<&Vector> for Point {
  type Output = Point;

  fn add(self, other: &Vector) -> Self::Output {
    Point::new(self.x + other.x, self.y + other.y)
  }
}

impl Add<Vector> for &Point {
  type Output = Point;

  fn add(self, other: Vector) -> Self::Output {
    Point::new(self.x + other.x, self.y + other.y)
  }
}

impl Add<Vector> for Point {
  type Output = Point;

  fn add(self, other: Vector) -> Self::Output {
    Point::new(self.x + other.x, self.y + other.y)
  }
}

impl Sub for &Vector {
  type Output = Vector;

  fn sub(self, other: Self) -> Self::Output {
    Vector { x: self.x - other.x, y: self.y - other.y }
  }
}

impl Mul<f64> for Vector {
  type Output = Vector;

  fn mul(mut self, scalar: f64) -> Self::Output {
    self.x *= scalar;
    self.y *= scalar;
    self
  }
}

impl Mul for &Vector {
  type Output = f64;

  fn mul(self, other: Self) -> Self::Output {
    self.x * other.x + self.y * other.y
  }

}

impl From<(f64, f64)> for Vector {
  fn from((x, y): (f64, f64)) -> Self {
    Self { x, y }
  }
}

impl From<(f64, f64)> for Point {
  fn from((x, y): (f64, f64)) -> Self {
    Self { x, y }
  }
}

impl From<&Point> for Point {
  fn from(p: &Point) -> Self {
    Self { x: p.x, y: p.y }
  }
}

impl num_traits::identities::Zero for Point {
  fn zero() -> Self {
    return Point::new(0.0, 0.0)
  }
  fn is_zero(&self) -> bool {
    return self.x == 0.0 && self.y == 0.0
  }

}

impl num_traits::identities::Zero for Vector {
  fn zero() -> Self {
    return Vector::new(0.0, 0.0)
  }
  fn is_zero(&self) -> bool {
    return self.x == 0.0 && self.y == 0.0
  }
}


fn use_scientific(x: f64) -> bool {
  if x == 0.0 {
    false
  } else if x.abs() < 0.0001 || x.abs() > 1000.0 {
    true
  } else {
    false
  }
}

impl std::fmt::Display for Vector {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
    if use_scientific(self.x) {
      if use_scientific(self.y) {
        write!(f, "({:+e}, {:+e})", self.x, self.y)
      } else {
        write!(f, "({:+e}, {:+})", self.x, self.y)
      }
    } else {
      if use_scientific(self.y) {
        write!(f, "({:+}, {:+e})", self.x, self.y)
      } else {
        write!(f, "({:+}, {:+})", self.x, self.y)
      }
    }
  }
}
