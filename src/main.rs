/*
TODO: New style simulation that progresses time by figuring out when next collision will happen.

1. You start by computing collision time for every pair of particles.
2. You figure out which collision happens soonest. Call that time t
3. Update all positions to reflect time t
4. Update the velocities of the particles in that collision.
5. Now update matrix of collision time, but only for collisions involving the two particles that just occured since the other collision times should be the same.
6. Repeat

This has the benefit of being relatively fast and efficient but also 100% correct.
*/
mod sim;
mod math;
mod test_cases;
mod runner;
mod update_hooks;

fn main() {
  runner::go();
}
