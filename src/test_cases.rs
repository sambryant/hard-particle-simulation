#![allow(dead_code)]

use rand::Rng;

use crate::math::Vector;
use crate::sim::*;
use crate::update_hooks;

pub fn test_realistic_gas() -> Simulation {
  let dt = 1.0 / 30.0;
  let t_end = 15.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 0.1;
  let num_parts = 5;

  let moving_bound_dir = Direction::West;
  let moving_bound_vel = 1.0;
  let hook = update_hooks::CollapsingBoundary::new(moving_bound_dir, moving_bound_vel);

  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let pos = vec![
    Vector::new(-7.5 + 0.015, 7.0),
    Vector::new(-7.5 + 0.010, 6.0),
    Vector::new(-7.5 + 0.005, 5.0),
    Vector::new(-7.5, 0.0),
    Vector::new(-7.5, -6.66),
  ];
  let vel = vec![
    Vector::new(-0.5, 0.0),
    Vector::new(-0.5, 0.0),
    Vector::new(-0.5, 0.0),
    Vector::new(0.0, 0.0),
    Vector::new(0.5, 0.0),
  ];
  let mut sim = Simulation::new(p, pos, vel);
  sim.add_hook(Box::new(hook));
  sim
}

pub fn test_moving_wall_single_particles() -> Simulation {
  let dt = 1.0 / 30.0;
  let t_end = 15.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 0.1;
  let num_parts = 5;

  let moving_bound_dir = Direction::West;
  let moving_bound_vel = 1.0;
  let hook = update_hooks::CollapsingBoundary::new(moving_bound_dir, moving_bound_vel);

  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let pos = vec![
    Vector::new(-7.5 + 0.015, 7.0),
    Vector::new(-7.5 + 0.010, 6.0),
    Vector::new(-7.5 + 0.005, 5.0),
    Vector::new(-7.5, 0.0),
    Vector::new(-7.5, -6.66),
  ];
  let vel = vec![
    Vector::new(-0.5, 0.0),
    Vector::new(-0.5, 0.0),
    Vector::new(-0.5, 0.0),
    Vector::new(0.0, 0.0),
    Vector::new(0.5, 0.0),
  ];
  let mut sim = Simulation::new(p, pos, vel);
  sim.add_hook(Box::new(hook));
  sim
}

pub fn test_moving_wall_error() -> Simulation {
  let dt = 1.0 / 30.0;
  let t_end = 15.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 0.1;
  let num_parts = 13;

  let moving_bound_dir = Direction::West;
  let moving_bound_vel = 1.0;
  let hook = update_hooks::CollapsingBoundary::new(moving_bound_dir, moving_bound_vel);

  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let pos = vec![
    Vector::new(-7.5 + 0.066, 15.0),
    Vector::new(-7.5 + 0.06, 14.0),
    Vector::new(-7.5 + 0.055, 13.0),
    Vector::new(-7.5 + 0.05, 12.0),
    Vector::new(-7.5 + 0.045, 11.0),
    Vector::new(-7.5 + 0.04, 10.0),
    Vector::new(-7.5 + 0.035, 9.0),
    Vector::new(-7.5 + 0.03, 8.0),
    Vector::new(-7.5 + 0.025, 7.0),
    Vector::new(-7.5 + 0.02, 6.0),
    Vector::new(-7.5 + 0.015, 5.0),
    Vector::new(-7.5 + 0.01, 4.0),
    Vector::new(-7.5 + 0.005, 3.0),
  ];
  let vel = vec![
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
    Vector::new(-5.0, 0.0),
  ];
  let mut sim = Simulation::new(p, pos, vel);
  sim.add_hook(Box::new(hook));
  sim
}

pub fn test_moving_wall_medium_gas() -> Simulation {
  let dt = 1.0 / 30.0;
  let t_end = 10.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 0.1;
  let n = 10;
  let num_parts = n * n;

  let moving_bound_dir = Direction::West;
  let moving_bound_vel = 1.0;
  let hook = update_hooks::CollapsingBoundary::new(moving_bound_dir, moving_bound_vel);

  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let mut rng = rand::thread_rng();
  let mut pos = Vec::new();
  let mut vel = Vec::new();
  let spacing = 20.0 / (n as f64);
  for i in 0..n {
    for j in 0..n {
      let vx = rng.gen_range(-1.0, 1.0);
      let vy = rng.gen_range(-1.0, 1.0);
      pos.push(Vector::new(-10.0 + spacing*(j as f64 + 0.5), -10.0 + spacing*(i as f64 + 0.5)));
      vel.push(Vector::new(vx, vy));
    }
  }
  let mut sim = Simulation::new(p, pos, vel);
  sim.add_hook(Box::new(hook));
  sim
}

pub fn test_moving_wall_fast_gas() -> Simulation {
  let dt = 1.0 / 30.0;
  let t_end = 60.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 0.1;
  let n = 40;
  let num_parts = n * n;

  let moving_bound_dir = Direction::West;
  let moving_bound_vel = 1.0;
  let hook = update_hooks::CollapsingBoundary::new(moving_bound_dir, moving_bound_vel);

  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let mut rng = rand::thread_rng();
  let mut pos = Vec::new();
  let mut vel = Vec::new();
  let spacing = 20.0 / (n as f64);
  for i in 0..n {
    for j in 0..n {
      let vx = rng.gen_range(-5.0, 5.0);
      let vy = rng.gen_range(-5.0, 5.0);
      pos.push(Vector::new(-10.0 + spacing*(j as f64 + 0.5), -10.0 + spacing*(i as f64 + 0.5)));
      vel.push(Vector::new(vx, vy));
    }
  }
  let mut sim = Simulation::new(p, pos, vel);
  sim.add_hook(Box::new(hook));
  sim
}

pub fn test_2_stationary() -> Simulation {
  let dt = 0.5;
  let t_end = 10.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 1.0;
  let num_parts = 2;
  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let pos = vec![
    Vector::new(-5.0, 0.0),
    Vector::new( 0.0, 0.0)
  ];
  let vel = vec![
    Vector::new(1.0, 0.0),
    Vector::new(0.0, 0.0)
  ];
  Simulation::new(p, pos, vel)
}

pub fn test_2_stationary_angle() -> Simulation {
  let dt = 0.5;
  let t_end = 10.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 1.0;
  let num_parts = 2;
  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let pos = vec![
    Vector::new(-3.0,-6.0),
    Vector::new( 0.0, 0.0)
  ];
  let vel = vec![
    Vector::new(0.5, 1.0),
    Vector::new(0.0, 0.0)
  ];
  Simulation::new(p, pos, vel)
}

pub fn test_2_stationary_angle_reverse() -> Simulation {
  let dt = 1.0/30.0;
  let t_end = 10.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 0.1;
  let num_parts = 2;
  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let pos = vec![
    Vector::new( 0.0, 0.0),
    Vector::new(-3.0,-6.0),
  ];
  let vel = vec![
    Vector::new(0.0, 0.0),
    Vector::new(0.5, 1.0),
  ];
  Simulation::new(p, pos, vel)
}

pub fn test_2_equal_collision_angle() -> Simulation {
  let dt = 0.5;
  let t_end = 10.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 1.3;
  let num_parts = 2;
  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let pos = vec![
    Vector::new( radius / (2.0_f64).sqrt(),  6.0),
    Vector::new(-radius / (2.0_f64).sqrt(), -6.0)
  ];
  let vel = vec![
    Vector::new(0.0, -1.0),
    Vector::new(0.0,  1.0)
  ];
  Simulation::new(p, pos, vel)
}

pub fn test_boundary() -> Simulation {
  let dt = 1.0/60.0;
  let t_end = 50.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 1.0;
  let num_parts = 1;
  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let pos = vec![
    Vector::new( 0.0, 5.0 )
  ];
  let vel = vec![
    Vector::new(2.0, 2.0)
  ];
  Simulation::new(p, pos, vel)
}


pub fn test_grid_random_velocity() -> Simulation {
  let dt = 1.0/60.0;
  let t_end = 100.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 1.0;
  let num_parts = 16;
  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let mut rng = rand::thread_rng();
  let mut pos = Vec::new();
  let mut vel = Vec::new();
  for i in 0..4 {
    for j in 0..4 {
      let vx = rng.gen_range(-1.0, 1.0);
      let vy = rng.gen_range(-1.0, 1.0);
      pos.push(Vector::new(-7.5 + 5.0 * (j as f64), -7.5 + 5.0 * (i as f64)));
      vel.push(Vector::new(vx, vy));
    }
  }
  Simulation::new(p, pos, vel)
}

pub fn test_grid_medium() -> Simulation {
  let dt = 1.0/60.0;
  let t_end = 20.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 0.1;
  let n = 10;
  let num_parts = n * n;
  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let mut rng = rand::thread_rng();
  let mut pos = Vec::new();
  let mut vel = Vec::new();

  let spacing = 20.0 / (n as f64);
  for i in 0..n {
    for j in 0..n {
      let vx = rng.gen_range(-1.0, 1.0);
      let vy = rng.gen_range(-1.0, 1.0);
      pos.push(Vector::new(-10.0 + spacing*(j as f64 + 0.5), -10.0 + spacing*(i as f64 + 0.5)));
      vel.push(Vector::new(vx, vy));
    }
  }
  Simulation::new(p, pos, vel)
}

pub fn test_isolated_error() -> Simulation {
  let dt = 1.0/60.0;
  let t_end = 2.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 0.5;
  let num_parts = 2;
  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let vel = vec![
    Vector::new(0.16767711, 0.99226841),
    Vector::new(0.30443133, -0.85031163)
  ];
  let pos = vec![
    Vector::new(-3.0, -1.0),
    Vector::new(-3.0,  1.0)
  ];
  Simulation::new(p, pos, vel)
}

pub fn test_gas() -> Simulation {
  let dt = 1.0/60.0;
  let t_end = 100.0;
  let bounds = [[-10.0, 10.0], [-10.0, 10.0]];
  let radius = 0.1;
  let num_parts = 30 * 30;
  let p = SimulationParameters::new(num_parts, dt, t_end, radius, bounds);

  let mut rng = rand::thread_rng();
  let mut pos = Vec::new();
  let mut vel = Vec::new();

  let spacing = 20.0 / 30.0;
  for i in 0..30 {
    for j in 0..30 {
      let vx = rng.gen_range(-1.0, 1.0);
      let vy = rng.gen_range(-1.0, 1.0);
      pos.push(Vector::new(-10.0 + spacing*(j as f64 + 0.5), -10.0 + spacing*(i as f64 + 0.5)));
      vel.push(Vector::new(vx, vy));
    }
  }
  Simulation::new(p, pos, vel)
}

// pub fn test_2_moving_together() -> (usize, f64, UpdateBuffer<Vec<Vector>>, Vec<Vector>) {
//   let num = 2;
//   let pos = vec![
//     Vector::new(0.0, 0.0),
//     Vector::new(2.0, 2.0)
//   ];
//   let vel = vec![
//     Vector::new(1.0, 1.0),
//     Vector::new(0.5, 0.5)
//   ];
//   let radius = 1.0;
//   (num, radius, UpdateBuffer::from(pos), vel)
// }
