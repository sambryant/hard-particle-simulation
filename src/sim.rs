#![allow(dead_code)]
use hdf5::types::VarLenUnicode;
use sb_rust_library::update_buffer::UpdateBuffer;
use ndarray;
use hdf5;

use crate::math::{Vector, mat_vec_mul3};

type Particle = usize;

#[derive(Clone, Copy, Debug)]
pub enum Direction {
  North, South, East, West
}

pub enum Collision {
  ParticleCollision(Particle, Particle, f64),
  BoundaryCollision(Particle, Direction, f64)
}

pub struct SimulationParameters {
  pub num_parts: usize,
  pub num_steps: usize,
  pub dt: f64,
  pub t_end: f64,
  pub radius: f64,
  pub bound_pos_init: [[f64; 2]; 2],
  pub bound_vel_init: [[f64; 2]; 2],
}

pub struct Output {
  pub time: ndarray::Array<f64, ndarray::Ix1>,
  pub pos: ndarray::Array<f64, ndarray::Ix3>,
  pub vel: ndarray::Array<f64, ndarray::Ix3>,
  pub bound_pos: ndarray::Array<f64, ndarray::Ix3>,
  pub bound_vel: ndarray::Array<f64, ndarray::Ix3>,
}

pub struct Simulation {
  p: SimulationParameters,
  pos: UpdateBuffer<Vec<Vector>>,
  vel: Vec<Vector>,
  time: f64,
  bound_pos: UpdateBuffer<[[f64; 2]; 2]>,
  pub bound_vel: [[f64; 2]; 2],
  update_hooks: Vec<Box<dyn SimulationUpdateHook>>,
}

pub trait SimulationUpdateHook {
  fn name(&self) -> String;
  fn update(&self, vel: &mut Vec<Vector>, bound_vel: &mut [[f64; 2]; 2]);
}

impl Simulation {

  pub fn new(p: SimulationParameters, pos: Vec<Vector>, vel: Vec<Vector>) -> Simulation {
    let bound_pos = UpdateBuffer::from(p.bound_pos_init);
    let bound_vel = p.bound_vel_init.clone();
    Simulation {
      p,
      vel,
      bound_pos,
      bound_vel,
      pos: UpdateBuffer::from(pos),
      time: 0.0,
      update_hooks: Vec::new(),
    }
  }

  pub fn run(mut self, filename: &str) {
    let mut flag = false;
    let mut output = Output::new(&self.p);
    for step in 0..self.p.num_steps {
      self.time = (step as f64) * self.p.dt;
      output.write(step, &self);

      if !flag {
        for i in 0..self.p.num_parts {
          if (self.pos.old()[i].x - self.p.radius) < self.bound_pos.old()[0][0] {
            println!("Problem collision found with particle {} at time {}", i, self.time);
            println!("  at {} but wall is {} (vel part {}, vel wall: {})", self.pos.old()[i], self.bound_pos.old()[0][0], self.vel[i], self.bound_vel[0][0]);
            flag = true;
          }
        }
      }


      self.advance_particles();
      let collisions = self.detect_collisions();
      self.handle_collisions(&collisions);
      self.pos.swap_buffers();
      self.bound_pos.swap_buffers();
      for hook in self.update_hooks.iter() {
        hook.update(&mut self.vel, &mut self.bound_vel);
      }
    }
    output.save(filename, &self).unwrap();
  }

  pub fn add_hook(&mut self, updater: Box<dyn SimulationUpdateHook>) {
    self.update_hooks.push(updater);
  }

  fn advance_particles(&mut self) {
    for i in 0..self.p.num_parts {
      self.pos.new()[i] = self.pos.old()[i] + self.vel[i] * self.p.dt;
    }
    // Update boundary positions
    self.bound_pos.new()[0][0] = self.bound_pos.old()[0][0] + self.p.dt * self.bound_vel[0][0];
    self.bound_pos.new()[0][1] = self.bound_pos.old()[0][1] + self.p.dt * self.bound_vel[0][1];
    self.bound_pos.new()[1][0] = self.bound_pos.old()[1][0] + self.p.dt * self.bound_vel[1][0];
    self.bound_pos.new()[1][1] = self.bound_pos.old()[1][1] + self.p.dt * self.bound_vel[1][1];
  }

  fn handle_collisions(&mut self, collisions: &Vec<Collision>) {

    for collision in collisions.iter() {
      match collision {
        Collision::ParticleCollision(i, j, f) => {
          // Compute particle position at time of collision
          let p1 = self.pos.old()[*i] + (self.vel[*i] * (*f));
          let p2 = self.pos.old()[*j] + (self.vel[*j] * (*f));
          let dr = &p2 - &p1;

          // Use this to compute the collision angle
          let angle = dr.y.atan2(dr.x);

          // Also compute the center of mass velocity.
          let v = (&self.vel[*i] + &self.vel[*j]) * 0.5;

          // Finally we construct the velocity reflection matrix.
          let c = angle.cos();
          let s = angle.sin();
          let c2s2 = c*c - s*s;
          let ccss = 2.0*c*s;

          let m = [
            [-c2s2, -ccss, v.x*(1.0 + c2s2) + v.y*ccss],
            [-ccss,  c2s2, v.y*(1.0 - c2s2) + v.x*ccss],
            [  0.0,   0.0,                         1.0]
          ];
          let v1 = [self.vel[*i].x, self.vel[*i].y, 1.0];
          let v2 = [self.vel[*j].x, self.vel[*j].y, 1.0];
          let v1_new = mat_vec_mul3(m, v1);
          let v2_new = mat_vec_mul3(m, v2);

          let mut net_dr1 = self.vel[*i] * (*f);
          let mut net_dr2 = self.vel[*j] * (*f);

          self.vel[*i].x = v1_new[0];
          self.vel[*i].y = v1_new[1];
          self.vel[*j].x = v2_new[0];
          self.vel[*j].y = v2_new[1];

          net_dr1 = net_dr1 + self.vel[*i] * (self.p.dt - f);
          net_dr2 = net_dr2 + self.vel[*j] * (self.p.dt - f);

          // Finally, update positions
          self.pos.new()[*i] = self.pos.old()[*i] + net_dr1;
          self.pos.new()[*j] = self.pos.old()[*j] + net_dr2;
        },
        Collision::BoundaryCollision(i, dir, f) => {
          // Advance position to moment of collision.
          self.pos.new()[*i] = self.pos.old()[*i] + (self.vel[*i] * *f);

          // Compute velocity after collision assuming boundary has infinite mass
          let coords = dir.coords();
          if dir.is_vertical() {
            self.vel[*i].y = 2.0 * self.bound_vel[coords.0][coords.1] - self.vel[*i].y;
          } else {
            self.vel[*i].x = 2.0 * self.bound_vel[coords.0][coords.1] - self.vel[*i].x;
          }

          // Advance position from moment of collision to end of time step
          self.pos.new()[*i] = self.pos.new()[*i] + (self.vel[*i] * (self.p.dt - f));

          if let Direction::West = dir {
            // if self.pos.new()[*i] - self.p.radius < self.bound_pos
          }
        }
      }
    }
  }

  fn detect_collisions(&mut self) -> Vec<Collision> {
    let mut collisions: Vec<Collision> = Vec::new();

    let bound0 = self.bound_pos.old().clone();
    let bound1 = self.bound_pos.new();

    let dis_thres = 4.0 * self.p.radius * self.p.radius;
    for i in 0..self.p.num_parts {
      let x0 = self.pos.old()[i].x;
      let y0 = self.pos.old()[i].y;
      let x1 = self.pos.new()[i].x;
      let y1 = self.pos.new()[i].y;

      // Check for boundary collisions
      if (bound1[0][0] - (x1 - self.p.radius)) > 0.0 {
        let f = (bound0[0][0] - x0 + self.p.radius) / (self.vel[i].x - self.bound_vel[0][0]);
        collisions.push(Collision::BoundaryCollision(i, Direction::West, f));
      }
      if ((x1 + self.p.radius) - bound1[0][1]) > 0.0 {
        let f = (bound0[0][1] - x0 - self.p.radius) / (self.vel[i].x - self.bound_vel[0][1]);
        collisions.push(Collision::BoundaryCollision(i, Direction::East, f));
      }
      if (bound1[1][0] - (y1 - self.p.radius)) > 0.0 {
        let f = (bound0[1][0] - y0 + self.p.radius) / (self.vel[i].y - self.bound_vel[1][0]);
        collisions.push(Collision::BoundaryCollision(i, Direction::South, f));
      }
      if ((y1 + self.p.radius) - bound1[1][1]) > 0.0 {
        let f = (bound0[1][1] - y0 - self.p.radius) / (self.vel[i].y - self.bound_vel[1][1]);
        collisions.push(Collision::BoundaryCollision(i, Direction::North, f));
      }
      // if x1 + self.p.radius > bound1[0][1] {
      //   let f = (bound0[0][1] - x0 - self.p.radius) / (self.vel[i].x - self.bound_vel[0][1]);
      //   collisions.push(Collision::BoundaryCollision(i, Direction::East, f));
      // } else if x1 - self.p.radius < bound1[0][0] {
      //   let f = (bound0[0][0] - x0 + self.p.radius) / (self.vel[i].x - self.bound_vel[0][0]);
      //   collisions.push(Collision::BoundaryCollision(i, Direction::West, f));
      // }
      // if y1 + self.p.radius > bound1[1][1] {
      //   let f = (bound0[1][1] - y0 - self.p.radius) / (self.vel[i].y - self.bound_vel[1][1]);
      //   collisions.push(Collision::BoundaryCollision(i, Direction::North, f));
      // } else if y1 - self.p.radius < bound1[1][0] {
      //   let f = (bound0[1][0] - y0 + self.p.radius) / (self.vel[i].y - self.bound_vel[1][0]);
      //   collisions.push(Collision::BoundaryCollision(i, Direction::South, f));
      // }

      // Check for collisions with other particles
      for j in (i + 1)..self.p.num_parts {
        let p2 = self.pos.new()[j];
        let dis = (x1 - p2.x) * (x1 - p2.x) + (y1 - p2.y) * (y1 - p2.y);
        if dis < dis_thres {
          // compute the time the collision occurs
          let dpos0 = &self.pos.old()[i] - &self.pos.old()[j];
          let dvel0 = &self.vel[i] - &self.vel[j];
          let dr20 = dpos0.x * dpos0.x + dpos0.y * dpos0.y;
          let dv20 = dvel0.x * dvel0.x + dvel0.y * dvel0.y;
          let drdv = dpos0.x * dvel0.x + dpos0.y * dvel0.y;
          let f = - (drdv/dv20) * (1.0 - (1.0 - dv20 * (dr20 - dis_thres)/(drdv * drdv)).sqrt());
          collisions.push(Collision::ParticleCollision(i, j, f));
        }
      }
    }
    collisions
  }
}

impl SimulationParameters {
  pub fn new(num_parts: usize, dt: f64, mut t_end: f64, radius: f64, bound_pos: [[f64; 2]; 2]) -> SimulationParameters {
    let num_steps = 1 + (t_end / dt) as usize;
    t_end = (num_steps as f64) * dt;
    SimulationParameters {
      num_parts, num_steps, dt, t_end, radius, bound_pos_init: bound_pos, bound_vel_init: [[0.0, 0.0], [0.0, 0.0]],
    }
  }
}

impl Output {
  pub fn new(params: &SimulationParameters) -> Output {
    use ndarray::{Array1, Array3};
    let num_steps = params.num_steps;
    let num_parts = params.num_parts;
    let time = Array1::zeros((num_steps,));
    let pos = Array3::zeros((num_steps, num_parts, 2));
    let vel = Array3::zeros((num_steps, num_parts, 2));
    let bound_pos = Array3::zeros((num_steps, 2, 2));
    let bound_vel = Array3::zeros((num_steps, 2, 2));
    Output {
      time, pos, vel, bound_pos, bound_vel,
    }
  }

  pub fn write(&mut self, index: usize, sim: &Simulation) {
    self.time[index] = sim.time;
    let pos = sim.pos.old();
    let bound = sim.bound_pos.old();
    for i in 0..sim.p.num_parts {
      self.pos[(index,i,0)] = pos[i].x;
      self.pos[(index,i,1)] = pos[i].y;
      self.vel[(index,i,0)] = sim.vel[i].x;
      self.vel[(index,i,1)] = sim.vel[i].y;
    }
    for i in 0..2 {
      self.bound_pos[(index, i, 0)] = bound[i][0];
      self.bound_pos[(index, i, 1)] = bound[i][1];
      self.bound_vel[(index, i, 0)] = sim.bound_vel[i][0];
      self.bound_vel[(index, i, 1)] = sim.bound_vel[i][1];
    }
  }

  pub fn save(&self, filename: &str, sim: &Simulation) -> Result<(), String> {
    use ndarray::arr1;
    let hooks: Vec<VarLenUnicode> = unsafe {
      sim.update_hooks
        .iter()
        .map(|u| VarLenUnicode::from_str_unchecked(u.name()))
        .collect()
    };

    let file = hdf5::File::create(filename)
      .map_err(|_| String::from("Couldn't create HDF5 file"))?;
    file.new_dataset::<f64>()
      .create("time", self.time.shape())
      .map_err(|e| format!("Couldn't initialize time dataset: {:?}", e))?
      .write(&self.time)
      .map_err(|e| format!("Couldn't write time dataset: {:?}", e))?;
    file.new_dataset::<f64>()
      .create("pos", self.pos.shape())
      .map_err(|e| format!("Couldn't initialize pos dataset: {:?}", e))?
      .write(&self.pos)
      .map_err(|e| format!("Couldn't write pos dataset: {:?}", e))?;
    file.new_dataset::<f64>()
      .create("vel", self.vel.shape())
      .map_err(|e| format!("Couldn't initialize vel dataset: {:?}", e))?
      .write(&self.vel)
      .map_err(|e| format!("Couldn't write vel dataset: {:?}", e))?;
    file.new_dataset::<f64>()
      .create("bound_pos", self.bound_pos.shape())
      .map_err(|e| format!("Couldn't initialize bound_pos: {:?}", e))?
      .write(&self.bound_pos)
      .map_err(|e| format!("Couldn't write bound_pos: {:?}", e))?;
    file.new_dataset::<f64>()
      .create("bound_vel", self.bound_vel.shape())
      .map_err(|e| format!("Couldn't initialize bound_vel: {:?}", e))?
      .write(&self.bound_vel)
      .map_err(|e| format!("Couldn't write bound_vel: {:?}", e))?;
    file.new_dataset::<VarLenUnicode>()
      .create("update_hooks", (sim.update_hooks.len(),))
      .map_err(|e| format!("Couldn't initialize update_hooks: {:?}", e))?
      .write(&arr1(&hooks))
      .map_err(|e| format!("Couldn't write bound_vel: {:?}", e))?;
    file.new_dataset::<f64>()
      .create("dt", (1,))
      .map_err(|e| format!("Couldn't initialize dt: {:?}", e))?
      .write(&arr1(&vec![sim.p.dt]))
      .map_err(|e| format!("Couldn't write dt: {:?}", e))?;
    file.new_dataset::<f64>()
      .create("t_end", (1,))
      .map_err(|e| format!("Couldn't initialize t_end: {:?}", e))?
      .write(&arr1(&vec![sim.p.t_end]))
      .map_err(|e| format!("Couldn't write t_end: {:?}", e))?;
    file.new_dataset::<f64>()
      .create("radius", (1,))
      .map_err(|e| format!("Couldn't initialize radius: {:?}", e))?
      .write(&arr1(&vec![sim.p.radius]))
      .map_err(|e| format!("Couldn't write radius: {:?}", e))?;
    Ok(())
  }
}

impl Direction {
  pub fn is_vertical(&self) -> bool {
    match self {
      Direction::West => false,
      Direction::East => false,
      Direction::North => true,
      Direction::South => true,
    }
  }
  pub fn coords(&self) -> (usize, usize) {
    match self {
      Direction::West => (0, 0),
      Direction::East => (0, 1),
      Direction::South => (1, 0),
      Direction::North => (1, 1),
    }
  }
}
