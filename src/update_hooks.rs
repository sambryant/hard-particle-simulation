use crate::math::Vector;
use crate::sim::{
  Direction,
  SimulationUpdateHook,
};

pub struct CollapsingBoundary {
  dir: Direction,
  vel: f64,
  coords: (usize, usize),
}

impl CollapsingBoundary {
  pub fn new(dir: Direction, vel: f64) -> CollapsingBoundary {
    CollapsingBoundary { dir, vel, coords: dir.coords() }
  }
}

impl SimulationUpdateHook for CollapsingBoundary {

  fn name(&self) -> String {
    format!("collapsing-boundary-{:?}-{}", self.dir, self.vel)
  }

  fn update(&self, _vel: &mut Vec<Vector>, bound_vel: &mut [[f64; 2]; 2]) {
    bound_vel[self.coords.0][self.coords.1] = self.vel;
  }
}

